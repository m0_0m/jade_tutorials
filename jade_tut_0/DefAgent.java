package jade_tut_0;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import java.util.concurrent.TimeUnit;
import jade.core.AID;

public class DefAgent extends Agent
{
    private int[] neighbor;
    private int numOfAgents;
    private int nCheCount = 0;
    private float number;
    private InfoPack info;

    @Override
    protected void setup(  )
    {
        Object[] args = this.getArguments();
        this.neighbor = (int[])args[0];
        this.numOfAgents = (int)args[1];
        Double dRand = Math.random(  ) * 10.0;
        this.number = dRand.intValue(  );		  // 'Случайное' число
        addBehaviour( new FindAver( this, TimeUnit.SECONDS.toMillis( 1 ) ) );
        this.info = new InfoPack( this.getLocalName(  ) + ":" + Float.toString( number ) );
    }

    public boolean knowAnswer(  ) {
        boolean know = false;
        if ( this.nCheCount > this.numOfAgents - 1 ) {
            number = info.getAverage(  );
            know = true;
        }
        if ( this.nCheCount == -1 ) { know = true; }
        return know;
    }

    public boolean listen(  )
    {
        boolean heardSomething = false;
        ACLMessage msg = this.receive();
        if ( this.nCheCount < this.numOfAgents && this.nCheCount != -1 ) {
            if (msg != null) {
                heardSomething = true;
                String content = msg.getContent(  );
                if ( content.contains( "answer:" ) ) {
                    this.number = Float.valueOf( content.split( ":" )[1] );
                    System.out.println("Agent " + this.getLocalName()
                            + " took the answer<" + this.number + ">"+this.nCheCount);
                    this.nCheCount = -1;
                } else {
                    if (!this.info.merge(content)) {
                        ++nCheCount;
                    } else {
                        nCheCount = 0;
                    }
                    System.out.println("Ag#" + this.getLocalName()
                            + " hv msg<" + content + ">"+this.nCheCount+"\n\t"
                            + this.info.toString(  ) );
                }
            }
        }
        return heardSomething;
    }

    public void talk(  )
    {
        ACLMessage msg = new ACLMessage( ACLMessage.INFORM );
        for ( Integer it : neighbor ) {
            AID receiverAID = new AID( Integer.toString(it),AID.ISLOCALNAME );
            msg.addReceiver( receiverAID );
        }
        if ( this.knowAnswer(  ) ) {
            msg.setContent( "answer:" + Float.toString( this.number ) );
        } else {
            msg.setContent(this.info.toString());
        }
        this.send( msg );
    }


    public void spellNum(  )
    {
        System.out.println( "\t\t\t\t\t\t\tAgent #" + this.getLocalName() + " counted: " + number );
    }
}
