package jade_tut_0;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

public class MainController
{
    private static int[][] topology = {
            /*1*/   { 2 },
            /*2*/   { 1, 3, 4 },
            /*3*/   { 2, 5, 6 },
            /*4*/   { 2, 5 },
            /*5*/   { 3, 4, 6 },
            /*6*/   { 3, 5 } };
    private static int numOfAgents = topology.length;

    void initAgents(  )
    {
        Runtime rt = Runtime.instance(  );

        Profile p = new ProfileImpl(  );
        p.setParameter( Profile.MAIN_HOST, "localhost" );
        p.setParameter( Profile.MAIN_PORT, "10097" );// [ 10087 - 10099 )
        p.setParameter( Profile.GUI, "false" );
        ContainerController cc = rt.createMainContainer( p );
        // Создание агентов и установка связи между ними
        try {
            for( int i = 1; i < MainController.numOfAgents + 1; ++i ) {
                System.out.println();
                Object[] args = {
                        topology[i - 1],
                        topology.length
                };
                AgentController agent = cc.createNewAgent(
                        Integer.toString(i),
                        "jade_tut_0.DefAgent",
                        args );
                agent.start(  );
            }
        } catch ( Exception e ) {
            e.printStackTrace(  );
        }
    }
}
