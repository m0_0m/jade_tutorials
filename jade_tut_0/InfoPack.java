package jade_tut_0;

import java.util.*;

public class InfoPack
{
    private String val = "";
    public String toString(  ) { return this.val; }
    public boolean merge(String str)
    {
        boolean changed = !this.val.equals( str );
        if ( changed ) {
            Map<Integer, Float> p1 = new HashMap<>(  ), p2 = new HashMap<>(  );
            String[] pair1 = val.split("/"), pair2 = str.split("/");
            for (String it : pair1) {
                String[] a = it.split(":");
                p1.put( Integer.valueOf( a[0] ), Float.valueOf( a[1] ) );
            }
            for (String it : pair2) {
                String[] a = it.split(":");
                p2.put( Integer.valueOf( a[0] ), Float.valueOf( a[1] ) );
            }
            changed = false;
            for ( Map.Entry<Integer, Float> it : p2.entrySet(  ) ) {
                if ( !p1.containsKey( it.getKey(  ) ) ) {
                    p1.put( it.getKey(  ), it.getValue(  ) );
                    changed = true;
                }
            }
            if ( changed ) {
                this.val = "";
                for ( Map.Entry<Integer, Float> it : p1.entrySet(  ) ) {
                    if ( this.val.length(  ) != 0 ) {
                        this.val = this.val.concat( "/" );
                    }
                    this.val = this.val.concat( it.getKey(  ).toString(  )
                        ).concat( ":" ).concat( it.getValue(  ).toString(  ) );
                }
            }
        }
        return changed;
    }
    public float getAverage(  )
    {
        String[] pair = this.val.split("/");
        float sum = 0.f;
        for ( String it : pair ) {
            sum += Float.valueOf( it.split( ":" )[1] );
        }
        return sum / pair.length;
    }
    public InfoPack( String str ) {
        val = str;
    }
}
