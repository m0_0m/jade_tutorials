package jade_tut_0;

//import jade.core.AID;
//import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
//import jade.lang.acl.ACLMessage;

public class FindAver extends TickerBehaviour
{
    private final DefAgent agent;
    private int currentStep;
    private final int MAX_STEPS = 30;

    FindAver( DefAgent agent, long period )
    {
        super( agent, period );
        this.setFixedPeriod( true );
        this.agent = agent;
        this.currentStep = 0;
    }

    @Override
    protected void onTick(  )
    {
        if ( currentStep < MAX_STEPS && ! this.agent.knowAnswer(  ) ) {
            this.agent.talk(  );
            while ( this.agent.listen(  )){}
            System.out.println( "Agent " + this.agent.getLocalName(  )
                                         + ": tick= " + getTickCount(  ) );
            this.currentStep++;
        } else {
            this.agent.talk(  );
            this.agent.spellNum(  );
            this.stop(  );
        }
    }
}
