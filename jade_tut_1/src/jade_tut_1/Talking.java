package jade_tut_1;
import jade.core.behaviours.TickerBehaviour;

import java.util.HashMap;



public class Talking extends TickerBehaviour
{
    private final DefAgent agent;

    Talking( DefAgent agent, long period ) {

        super( agent, period );
        this.setFixedPeriod( true );
        this.agent = agent;
    }

    @Override
    protected void onTick(  )
    {
        boolean lookAtAgent =
					Integer.parseInt(this.agent.getLocalName()) == DefAgent.lookAt;

        MainController.changeTopology( lookAtAgent );

        if ( !this.agent.knowAnswer(  ) && this.agent.hasNoChange < 700 ) {

            this.agent.talk(  );
            while ( this.agent.listen(  )){}
						this.agent.think();
            if ( lookAtAgent ) this.agent.spellN(  );
            if ( lookAtAgent ) {
                System.out.println( " ~ ~ ~ ~ ~ " + MainController.noise );
                System.out.print( "Agent " + this.agent.getLocalName( ) + "\n" );
            }
        } else {
            this.agent.spellNum(  );
            float sum = 0.f;
            for( Float it : DefAgent.guessValues ) {
                sum += it;
            }
            sum /= DefAgent.guessValues.length;
            this.agent.stop = true;
            System.out.println( "\t\t\t\tAnd it actually should be: " + sum );
            Integer i = Integer.parseInt( this.agent.getLocalName( ) );
            this.stop(  );
        }
    }
}
