package jade_tut_1;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import jade.core.AID;



public class DefAgent extends Agent
{
	// Номер агента, данные которого будут выводиться в консоль
  public static int lookAt = 1;

  public static float[] guessValues = new float[6];
  public boolean stop = false;
  private int[] neighbor;
  private int numOfAgents;
  private float number;
  private InfoPack info; // данные, в которые мы верим
  private HashMap<Integer, List<Float>> memory;
  private HashMap<Integer, Integer> N;
  private HashMap<Integer, Float> result;
  private HashMap<Integer, Boolean> ready;
  private boolean yMessage = false;
  private final float POROG = 0.01f;
  protected long hasNoChange;
  
  
  
  @Override
  protected void setup(  )
  {
    this.hasNoChange = 0;
    Object[] args = this.getArguments();
    this.neighbor = (int[])args[0]; // Ссылка на инфу, с кем может связаться
    this.numOfAgents = (int)args[1];// Сколько соседей всего
  
    Double dRand = Math.random(  ) * 10.0;
    this.number = dRand.intValue(  );		  // 'Случайное' число
    guessValues[Integer.parseInt( this.getLocalName(  ) ) - 1] = this.number;
  
    addBehaviour( new Talking( this, TimeUnit.MILLISECONDS.toMillis( 10 ) ) );
  
    this.memory = new HashMap<>(  );
    this.N = new HashMap<>(  );
    this.result = new HashMap<>(  );
    this.ready = new HashMap<>(  );
    this.yMessage = false;
    
    this.memory.put(Integer.parseInt( this.getLocalName() ), new ArrayList<>() );
    memory.get( Integer.parseInt( this.getLocalName() ) ).add( number );
    
    this.result.put( Integer.parseInt( this.getLocalName(  ) ), number );
    this.info = new InfoPack( "", result );
    
    this.spellNum();
  }
  
  
  
  public boolean knowAnswer(  )
  {
    boolean know = false;
    if ( !ready.isEmpty(  ) ) {
      for ( Boolean it : ready.values( ) ) {
        know = true;
        if ( !it ) know = false;
      }
    }
    return know;
  }
  
  
  public int hear( HashMap<Integer, Integer> table )
  {
    int heardSomething = -1;
    ACLMessage msg = this.receive();
    
    if (msg != null) {
      
      heardSomething = 2;
      String content = msg.getContent(  );
      
      if ( content.contains( "!" ) ) { 
        String[] aContent = content.split( "!" );
        Integer jt = Integer.parseInt( aContent[1] );
        content = aContent[0];
        table.put( jt, 0 );
        heardSomething = 0;
      } else if ( content.contains( "y" ) ) {
        String[] aContent = content.split( "y" );
        Integer jt = Integer.parseInt( aContent[1] );
        content = aContent[0];
        table.put( jt, 1 );
        heardSomething = 1;
      }
  
      this.info.perceive( content, memory, false );
      
    }
    
    return heardSomething;
  }
  
  public boolean listen(  )
  {
    boolean heardSomething = false;
    ACLMessage msg = this.receive();
      ++hasNoChange;
      if (msg != null) {
        hasNoChange = 0;
        heardSomething = true;
        String content = msg.getContent(  );
        
        if ( content.contains( "!" ) ) { content = content.split( "!" )[0]; }
        if ( content.contains( "y" ) ) { content = content.split( "y" )[0]; this.yMessage = true; }
        
        boolean lookAtAgent = Integer.parseInt( this.getLocalName() ) == DefAgent.lookAt;
        
        this.info.perceive( content, memory, lookAtAgent );
        
        if ( lookAtAgent ) {
          System.out.println( "Ag#" + this.getLocalName( ) + " hv msg<" + content + ">" );
        }
      }
    
    return heardSomething;
  }
  
  
  
  
  public void think(  )
  {
    // обрабатываем данные пришедшие в память
    if ( !this.memory.isEmpty(  ) ) {
      for ( Integer it : this.memory.keySet( ) ) { // для каждого узнанного соседа
        if ( !ready.containsKey( it ) ) {
          ready.put( it, false );
        }
        if ( !ready.get( it ) ) {
          // Если есть новое N-ое писемо о весе соседа,
          if ( !this.memory.get( it ).isEmpty( ) ) {
            final int buffSize = 1;
            float Wn = 0.f;
            int n = this.memory.get( it ).size( );
            for ( int i = 0; i < n; ++i ) {
              float v = this.memory.get( it ).get( i );
              Wn += v;
            }
            if ( this.result.containsKey( it ) ) {
              boolean more = false;
              if ( N.containsKey( it ) ) {
                more = n >= buffSize;
                if ( more ) {
                  int nn = N.get( it );
                  float Wo = this.result.get( it ) * nn;
                  Wn = ( Wn + Wo ) / (float) ( nn + n );
                }
              }
              if ( !more ) {
                Wn += this.result.get( it );
                Wn /= (float) ( n + 1 );
              }
            } else {
              Wn /= (float) n;
            }
            Wn = (float) Math.sqrt( Math.round( Wn * Wn ) );
            this.result.put( it, Wn );
            if ( !this.result.containsKey( it ) || n >= buffSize ) {
              memory.get( it ).clear( );
              if ( N.containsKey( it ) ) { N.put( it, N.get( it ) + n ); }
              else { N.put( it, n ); }
            }
            if ( N.containsKey( it ) ) {
              if ( N.get( it ) > 100 ) {
                ready.put( it, true );
              }
            }
          }
        } else { memory.get( it ).clear(  ); }
      }
    }
  }
  
  
  
  
  public void talk(  )
  {
    ACLMessage msg = new ACLMessage( ACLMessage.INFORM );
    for ( int i = 0; i < this.numOfAgents; ++i ) {
      if ( this.neighbor[i] != 0 ) {
        AID receiverAID = new AID( Integer.toString((i + 1)), AID.ISLOCALNAME );
        msg.addReceiver( receiverAID );
      }
    }
    String str = this.info.toString(  );
    if ( this.stop ) {
      str = str.concat( "y" ).concat( this.getLocalName(  ) );
    } else if ( this.yMessage ) {
      str = str.concat( "!" ).concat( this.getLocalName(  ) );
    }
    msg.setContent( str );
    this.send( msg );
  }
  
  
  
  public void spellWhatReady(  ) {
    System.out.println( "---------------------------" );
    for ( HashMap.Entry<Integer, Boolean> it : this.ready.entrySet() ) {
      System.out.println( "" + it.getKey() + " : " + it.getValue() );
    }
    System.out.println( "---------------------------" );
  }
  
  
  public void spellResult(  ) {
    System.out.println( "---------------------------" );
    for ( HashMap.Entry it : this.result.entrySet() ) {
      System.out.println( "" + it.getKey() + " : " + it.getValue() );
    }
    System.out.println( "---------------------------" );
  }
  
  public void spellMemory(  ) {
    System.out.print( "\n---------------------------" );
    for ( HashMap.Entry<Integer, List<Float>> it : this.memory.entrySet() ) {
      System.out.print( "\n" + it.getKey() + " : ");
      
      for ( int i = 0; i < it.getValue().size(); ++i ) {
        System.out.print( it.getValue(  ).get( i ) + ", " );
      }
    }
    System.out.println( "\n---------------------------" );
  }
  
  public void spellN(  ) {
    System.out.println( "---------------------------" );
    for ( HashMap.Entry<Integer, Integer> it : this.N.entrySet() ) {
      System.out.println( "" + it.getKey() + " : " + it.getValue() );
    }
    System.out.println( "---------------------------" );
  }
  
  public void spellNum(  )
  {
    System.out.println( "\t\t\t\t\t\t\tAgent #" + this.getLocalName() + " counted: " + this.info.getAverage() );
  }
}
