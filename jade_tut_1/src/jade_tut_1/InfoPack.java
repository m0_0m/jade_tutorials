package jade_tut_1;
import java.util.*;



public class InfoPack
{
    public String toString(  )
		{
        this.update(  );
        return this.val;
    }
    
    public boolean perceive( String str, HashMap<Integer, List<Float>> memory, boolean chosen)
    {
        String[] pair_str = str.split( "/" );
        boolean changed = false;
        for ( String it : pair_str ) {
            String[] firstAndSecond = it.split( ":" );
            Integer key = Integer.parseInt( firstAndSecond[0] );
            Float value = Float.parseFloat( firstAndSecond[1] );
            if ( memory.containsKey( key ) ) {
                memory.get( key ).add( value );
            } else {
                memory.put( key, new ArrayList<>(  ) );
                memory.get( key ).add( value );
                changed = true;
            }
        }
        return changed;
    }
    
    private String val = "";
    private HashMap<Integer, Float> hmap;
    public InfoPack( String str, HashMap<Integer, Float> hmap ) {
        val = str;        // инициализация строкой
        this.hmap = hmap; // инициализация ссылкой
    }
    public void update(  ) {
        if ( !hmap.isEmpty(  ) ) {
            StringBuilder str = new StringBuilder( );
            for ( HashMap.Entry<Integer, Float> it : hmap.entrySet( ) ) {
                if (it.getValue() != null) {
                    str.append( it.getKey( ) ).append( ":" );
                    str.append( it.getValue( ) + MainController.noise ).append( "/" );
                }
            }
            str.delete( str.length( ) - 1, str.length( ) );
            this.val = str.toString( );
        }
    }
    public float getAverage(  ) {
        float sum = 0.f;
        for ( Float it : hmap.values() ) { sum += it; }
        return sum / hmap.size();
    }
}
