package jade_tut_1;

import java.util.ArrayList;
import java.util.Random;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;




public class MainController
{
    private static int[][] initialTopology = {
            /*        1  2  3  4  5  6  */
            /*1 */  { 0, 1, 0, 0, 0, 0 },
            /*2 */  { 1, 0, 1, 1, 0, 0 },
            /*3 */  { 0, 1, 0, 0, 1, 1 },
            /*4 */  { 0, 1, 0, 0, 1, 0 },
            /*5 */  { 0, 0, 1, 1, 0, 1 },
            /*6 */  { 0, 0, 1, 0, 1, 0 } };

    private static int[][] topology = {
        /*        1  2  3  4  5  6  */
        /*1 */  { 0, 1, 0, 0, 0, 0 },
        /*2 */  { 1, 0, 1, 1, 0, 0 },
        /*3 */  { 0, 1, 0, 0, 1, 1 },
        /*4 */  { 0, 1, 0, 0, 1, 0 },
        /*5 */  { 0, 0, 1, 1, 0, 1 },
        /*6 */  { 0, 0, 1, 0, 1, 0 } };
    protected static double noise = 0.f;
    private static int numOfAgents = topology.length;
    private static Random rand = new Random(  );

    void initAgents(  )
    {
        Runtime rt = Runtime.instance(  );

        Profile p = new ProfileImpl(  );
        p.setParameter( Profile.MAIN_HOST, "localhost" );
        p.setParameter( Profile.MAIN_PORT, "10097" );// [ 10087 - 10099 )
        p.setParameter( Profile.GUI, "false" );
        ContainerController cc = rt.createMainContainer( p );

        // Создание агентов и установка связи меж ними
        try {

            for( int i = 1; i < MainController.numOfAgents + 1; ++i ) {

                System.out.println();
                
                Object[] args = { topology[i - 1], topology.length };
                
                AgentController agent = cc.createNewAgent(
                    Integer.toString(i),"jade_tut_1.DefAgent", args );
                
                agent.start(  );
            }

        } catch ( Exception e ) { e.printStackTrace(  ); }
        
        System.out.println( "End init Agents" );
    }
    
    
    public static void changeTopology( boolean needPrint )
    {
        double nbuff;
        while( Math.abs(nbuff = rand.nextGaussian(  )) >= 1.0 ){}
        noise = nbuff;
        
        // Изменение топологии
        if ( needPrint ) System.out.print("\n");
        for ( int i = 0; i < topology.length; ++i ) {
            for ( int j = 0; j < topology[i].length; ++j ) {
                if ( initialTopology[i][j] == 1 ) {
                    topology[i][j] = ((i == 5 && j == 4)||(i == 4 && j == 5))?(rand.nextInt( 5 )!=0 ? 1 : 0):1;
                }
                if ( needPrint ) {
                    if ( j != 0 ) System.out.print( ", " );
                    System.out.print( topology[i][j] );
                }
            }
            if ( needPrint ) System.out.print("\n");
        }
    }
}
